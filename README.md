# EDA Gobernance Catalog

This project contains all the metadata used by the EDA Catalog. Through this project all the EDA infrastructure is cataloged, governed and deployed.

---

## Official EDA Documentation

All the documentation this < confluent space > covers topics like:

- Infrastructure
- Design Patterns
- Technical Labs
- Technical Resources
- Security
- Authentication and Authorization rules
- Billing Model
- Platform Monitoring
- ...

---

## Catalog Structure

```bash
.
├── COMPANY
│   ├── BUSINESS UNIT
│   │   ├── PROJECT
│   │   │   ├── ASSET
│   │   │   │   ├── Components
│   │   │   │   │   └── components.yml
│   │   │   │   └── Infrastructure
│   │   │   │       ├── CLUSTER
│   │   │   │       │   ├── infrastructure.yml
│   │   │   │       │   └── README.md
│   │   │   └── README.md
│   │   ├── project_form.yml
│   │   └── README.md
│   └── README.md
└── README.md
```

---

## Project Sign Up & Approval Procedure

< Procedure or confluent link>

---

## Project Approval Procedure

< Procedure or confluent link>

---

## Request an Environment

< Procedure or confluent link>

---

## Request a Confluent cluster

< Procedure or confluent link>

---

## Request a Confluent resource: topic, ksqldb table...

< Procedure or confluent link>

---

## Request access to a thrid party resource: topic, ksqldb table...

< Procedure or confluent link>

---

## Accept an access request from a third party 

< Procedure or confluent link>