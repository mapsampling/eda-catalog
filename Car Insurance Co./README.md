# Car Insurance Co.

< Company summary desc >

---

## Billing Contacts

- Contact Name A < corporate@email.com >
    - Contact details (position, responsabilities, etc...)
- Contact Name B < corporate@email.com >
    - Contact details (position, responsabilities, etc...)
- Department Name < corporate@email.com >
    - Contact details (department functions, responsabilities, etc..)

