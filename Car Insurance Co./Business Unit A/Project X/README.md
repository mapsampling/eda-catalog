# < Project/Service Name >

< Project/Service Description >

## Project One Shot Architecture

< Describe the project architecture in one or two images >

---

## Project Catalog Form

The project required info should be available and maintained in the following local yaml file:

- **[project_form.yml](project_form.yml)**