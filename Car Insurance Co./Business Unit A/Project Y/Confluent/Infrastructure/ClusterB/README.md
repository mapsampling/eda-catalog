# < Cluster Name >

< Cluster Description >

## Cluster form

The cluster required info should be available and maintained in the following local yaml file:

- **[infrastructure.yml](infrastructure.yml)**

Changes on this file can produce a deployment process